package models

// Response ..
type Response struct {
	Data interface{} `json:"data"`
	Meta Meta        `json:"meta"`
}

//Meta ...
type Meta struct {
	Status  bool   `json:"status"`
	Code    int    `json:"code"`
	Message string `json:"message"`
}
