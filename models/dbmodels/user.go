package dbmodels

type User struct {
	ID     int    `gorm:"id";pk json:"id"`
	Nama   string `gorm:"nama" json:"nama"`
	Phone  string `gorm:"phone" json:"phone"`
	Addres string `gorm:"addres" json:"addres"`
}

func (t *User) TableName() string {
	return "public.tb_marchent1"
}
