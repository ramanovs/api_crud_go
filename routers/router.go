// @APIVersion 1.0.0
// @Title beego Test API
// @Description beego has a very cool tools to autogenerate documents for your API
// @Contact astaxie@gmail.com
// @TermsOfServiceUrl http://beego.me/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"learnbeegoAPI/controllers"

	"github.com/astaxie/beego"
)

func init() {
	ns := beego.NewNamespace("/v1",
		beego.NSRouter("/UserList", &controllers.UserController{}, "get:Userlist"),
		beego.NSRouter("/add_user", &controllers.UserController{}, "post:Adduser"),
		beego.NSRouter("/delet_user/:id", &controllers.UserController{}, "delete:Deletuser"),
		beego.NSRouter("/update_user/:id", &controllers.UserController{}, "put:Updateuser"),

		beego.NSNamespace("/object",
			beego.NSInclude(
				&controllers.ObjectController{},
			),
		),
		beego.NSNamespace("/user",
			beego.NSInclude(
				&controllers.UserController{},
			),
		),
	)

	beego.AddNamespace(ns)
}
