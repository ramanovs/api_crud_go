package services

import (
	"errors"
	"learnbeegoAPI/models"

	// "learnbeego1/models/dbmodels"
	db "learnbeegoAPI/database"
	"learnbeegoAPI/utils"

	"github.com/astaxie/beego/logs"
)

// func UserList() models.Response {
// 	var resp string = "berhasil"
// 	logs.Info("masuk services userlist")
// 	res := models.Response{}

// 	// resp := db.GetUser()

// 	//////////
// 	res = models.Response{
// 		Meta: utils.ResponseMetaOK(),
// 		Data: models.UserResponse{
// 			Nama: resp,
// 		},
// 	}
// 	return res
// }

func UserList() models.Response {

	logs.Info("masuk services userlist")
	// deklarasi response
	res := models.Response{}

	// service exce
	data, err := db.GetUser()

	if err != nil {
		logs.Info("Internal Server Error")
		return res
	}

	a := []models.UserResponse{}
	for _, val := range data {
		label := models.UserResponse{
			Nama:    val.Nama,
			Address: val.Addres,
			Phone:   val.Phone,
		}
		a = append(a, label)
	}

	// result JSON
	res = models.Response{
		Meta: utils.ResponseMetaOK(),
		// Data: models.UserResponse{
		// 	Nama:  data.Nama,
		// 	Phone: data.Phone,
		// 	Email: data.Email,
		// },
		Data: a,
	}
	return res
}

func AddUser(req models.UserReq) models.Response {
	logs.Info("masuk services add user")

	res := models.Response{}
	_, err := db.SaveUser_DB(req.Nama, req.Address, req.Phone)

	if err != nil {
		logs.Info("Gagal Save ke Database", err)
	}

	// res = models.Response{
	// 	Meta: utils.GetMessageSuccess(res, 200, errors.New("Voucher tidak ditemukan")),
	// 	Data: models.UserResponse{
	// 		Nama:    req.Nama,
	// 		Address: req.Address,
	// 		Phone:   req.Phone,
	// 	},
	// }

	res = utils.GetMessageSuccess(res, 200, errors.New("User berhasil di tambah"))
	return res
}

func DeletUser(id int) models.Response {
	res := models.Response{}
	//db.Unscoped().Where("age = 20").Find(&users)
	// tbTrns := dbmodels.User{}

	// err := db.Dbcon.Unscoped().Where("id =?", id).Delete(&tbTrns).Error
	// logs.Info("ini err", err)

	_, err := db.DeletUser_DB(id)

	if err != nil {
		logs.Info("Gagal Delete Data", err)
		res = utils.GetMessageFailedError(res, 401, errors.New("gagal delet"))
	} else {
		res = utils.GetMessageSuccess(res, 200, errors.New("User berhasil di remove"))
	}
	//res = utils.GetMessageSuccess(res, 200, errors.New("User berhasil di remove"))
	return res

}

func UpdateUser(id int, req models.UserReq) models.Response {
	logs.Info("masuk service update user")

	res := models.Response{}

	_, err := db.UpdatuserDB(id, req.Address, req.Nama, req.Phone)

	if err != nil {
		logs.Info("Gagal Save ke Database", err)
	}

	res = utils.GetMessageSuccess(res, 200, errors.New("User berhasil di update"))
	return res

}
