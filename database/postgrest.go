package database

import (
	"fmt"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var (
	Dbcon    *gorm.DB
	Errdb    error
	dbuser   string
	dbpass   string
	dbname   string
	dbaddres string
	dbport   string
	dbdebug  bool
	dbtyep   string
	sslmode  string
)

func init() {
	fmt.Println("DB POSTGRES")

	dbuser = beego.AppConfig.DefaultString("db.postgres.user", "postgres")
	dbpass = beego.AppConfig.DefaultString("db.postgres.pass", "postgres")
	dbname = beego.AppConfig.DefaultString("db.postgres.name", "postgres")
	dbaddres = beego.AppConfig.DefaultString("db.postgres.addres", "localhost")
	dbport = beego.AppConfig.DefaultString("db.postgres.port", "5433")
	sslmode = beego.AppConfig.DefaultString("db.postgres.sslmode", "disable")
	dbdebug = beego.AppConfig.DefaultBool("db.postgres.debug", true)
	if DbOpen() != nil {
		//panic("DB Can't Open")
		fmt.Println("Can Open db Postgres")
	}
	// 45.126.132.61
	Dbcon = GetDbCon()
	Dbcon = Dbcon.LogMode(dbdebug)

}

// DbOpen ..
func DbOpen() error {
	args := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s", dbaddres, dbport, dbuser, dbpass, dbname, sslmode)
	Dbcon, Errdb = gorm.Open("postgres", args)

	if Errdb != nil {
		logs.Error("open db Err ", Errdb)
		return Errdb
	}

	if errping := Dbcon.DB().Ping(); errping != nil {
		return errping
	}
	return nil
}

// GetDbCon ..
func GetDbCon() *gorm.DB {
	//TODO looping try connection until timeout
	// using channel timeout
	if errping := Dbcon.DB().Ping(); errping != nil {
		logs.Error("Db Not Connect test Ping :", errping)
		errping = nil
		if errping = DbOpen(); errping != nil {
			logs.Error("try to connect again but error :", errping)
		}
	}
	Dbcon.LogMode(dbdebug)
	return Dbcon
}
