package database

import (
	"fmt"
	"learnbeegoAPI/models/dbmodels"

	"github.com/astaxie/beego/logs"
)

func GetUser() ([]dbmodels.User, error) {
	res := []dbmodels.User{}

	// err := Dbcon.Find(&res).Error
	// err := Dbcon.Exec(`select * from users`).Scan(&res).Error
	query := fmt.Sprintf("select * from tb_marchent1")
	sql := Dbcon.Raw(query).Scan(&res)

	if sql.Error != nil {
		logs.Info("Failed to Checking from database", sql.Error)
		return res, sql.Error
	}
	logs.Info("data di table user :", res)

	return res, nil

}

func SaveUser_DB(Nama, Addres, Phone string) (dbmodels.User, error) {
	res := dbmodels.User{}
	//err := Dbcon.Exec(`INSERT INTO tb_marchent1 (nama, addres, phone) VALUES (nama =  ?, addres = ?, phone = ?)`, Nama, Addres, Phone).Scan(&res).Error
	label := dbmodels.User{
		Nama:   Nama,
		Addres: Addres,
		Phone:  Phone,
	}

	err := Dbcon.Create(&label).Error

	if err != nil {
		logs.Info("Failed Save to database", err)
		return res, err
	}

	return res, nil

}

func DeletUser_DB(id int) (dbmodels.User, error) {
	res := dbmodels.User{}
	tbTrns := dbmodels.User{}

	err := Dbcon.Unscoped().Where("id =?", id).Delete(&tbTrns).Error
	logs.Info("ini value tbTrns", tbTrns)
	logs.Info("ini error", err)

	if err != nil {
		logs.Info("Failed Delet data", err)
		return res, err
	}
	return res, nil
}

func UpdatuserDB(id int, Nama, Addres, Phone string) (dbmodels.User, error) {

	res := dbmodels.User{}
	tbTrns := dbmodels.User{}

	// db.Table("users").Where("id IN (?)", []int{10, 11}).Updates(map[string]interface{}{"name": "hello", "age": 18})
	// db.Model(&user).Updates(User{Name: "hello", Age: 18})

	err := Dbcon.Model(tbTrns).Unscoped().Where("id =?", id).Updates(map[string]interface{}{"nama": Nama, "addres": Addres, "phone": Phone}).Error
	// err := Dbcon.Table(tbTrns).Where("id IN (?)", []int{id}).Updates(map[string]interface{}{"nama": Nama, "addres": Addres, "phone": Phone}).Error
	if err != nil {
		logs.Info("Failed Save update to database", err)
		return res, err
	}

	return res, nil

}

// func GetDataOptionsPulsa(OperatorCode int) ([]dbmodels.PPOBOptions, error) {
// 	resp := []dbmodels.PPOBOptions{}

// 	query := fmt.Sprintf("select * from ppob_options where transaction_type_name = 'Pulsa HP' AND operator_code = %d AND product_type = 14 order by product_code", OperatorCode)
// 	sql := Dbcon.Raw(query).Scan(&resp)
// 	fmt.Println("=========================================")
// 	fmt.Println("Data Options : ", resp)
// 	fmt.Println("=========================================")
// 	if sql.Error != nil {

// 		logging.Error(logmodels.LogRequest{
// 			State:       "[ERROR-SQL]",
// 			Packages:    "[db]",
// 			Function:    "[GetDataOptionsPulsa]",
// 			RequestData: fmt.Sprintf("query : %s", query),
// 			RawMessage:  "Failed",
// 		})
// 	}

// 	return resp, nil
// }
