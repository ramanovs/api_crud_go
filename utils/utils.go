package utils

import "learnbeegoAPI/models"

func ResponseMetaOK() models.Meta {
	return models.Meta{
		Status:  true,
		Code:    200,
		Message: "SUCCESS",
	}
}

func GetMessageFailedError(res models.Response, code int, err error) models.Response {
	res = models.Response{}

	res.Meta.Code = code
	res.Meta.Status = false
	res.Meta.Message = err.Error()

	return res
}

func GetMessageSuccess(res models.Response, code int, err error) models.Response {

	res = models.Response{}

	res.Meta.Code = code
	res.Meta.Status = true
	res.Meta.Message = err.Error()

	return res
}
